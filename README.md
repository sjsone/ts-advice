# Aspect Oriented Programming

## Getting started

At first create a [managed class](#managed-classes) using the `@Managed()` decorator.

```typescript
@Managed('Foo')
class ManagedExampleClass {
    a: string = 'aaa'
    b: string

    constructor(b: string = 'b') {
        this.b = b
    }

    getValue(additional: string = '') {
        return this.a + this.b + additional
    }

    upperCased(str: string) {
        return str.toUpperCase()
    }
}
```

Then create an [aspect](#aspect).

```typescript
@Aspect
class TestAspect {
  	protected prefix = 'from_cache'
  	protected cache = new Map<string, any>()
  
    @Before(test, '->getValue()')
    beforeGetValue(jointPoint: BeforeJointPoint) {
        jointPoint.setArguments(['set from before'])
    }

    @After(ManagedExampleClass, '->getValue()')
    afterGetValue(jointPoint: AfterJointPoint) {
        return jointPoint.getLastReturn().toUpperCase()
    }

    @Around('Foo->upperCased()')
    cache(jointPoint: AroundJointPoint) {
        const id = jointPoint.getArguments().join('_')
        if (this.cache.has(id)) {
            return jointPoint.break(this.prefix + ':' + this.cache.get(id))
        }
        const ret = jointPoint.proceed()
        this.cache.set(id, ret)
        return ret
    }
}
```

Using the ManagedClass

```typescript
// @Managed('Foo')
// class ManagedExampleClass { ... }

const test = new ManagedExampleClass('ccc')

// class TestAspect { ... }

console.log(test.getValue('ddd'))     // prints: AAACCCSET FROM BEFORE
console.log(test.upperCased('asdf'))  // prints: ASDF
console.log(test.upperCased('asdfa')) // prints: ASDFA
console.log(test.upperCased('asdf'))  // prints: from_cache:ASDF
```



## Managed Classes

Aspects can only be applied to `@Managed(entryIdentifier?: string)` classes. The optional `entryIdentifier` is used to assign a unique name to a class. If no `entryIdentifier` is provided the class name gets used. 

### Example

```typescript
@Managed('Foo')
class ManagedClass {
    a: string = 'aaa'
    b: string

    constructor(b: string = 'b') {
        this.b = b
    }

    getValue(additional: string = '') {
        return this.a + this.b + additional
    }

    upperCased(a: string) {
        return a.toUpperCase()
    }
}
```

## Aspect

### Expressions

It is possible to declare an Aspect in three different ways.

Using an instance of a managed class:

```typescript
@Before(instanceOfManagedExampleClass, '->getValue()')
```

Using the managed class itself:

```typescript
@Before(ManagedExampleClass, '->getValue()')
```

Using the identifier defined with the `@Managed('Foo')` annotation or the Classname if no identifier is provided

```typescript
// @Managed('Foo')
// class ManagedExampleClass { }
// ...
@Before('Foo->getValue()')
```

```typescript
// @Managed()
// class ManagedExampleClass { }
// ...
@Before('ManagedExampleClass->getValue()')
```

# JointPoints
The JointPoints are mainly used for typing the methods argument
## BeforeJointPoint
```typescript
@Before('Foo->getValue()')
beforeGetValue(jointPoint: BeforeJointPoint)
```
## AfterJointPoint
```typescript
@After('Foo->getValue()')
afterGetValue(jointPoint: AfterJointPoint)
```
## AroundJointPoint

```typescript
@Around('Foo->upperCased()')
aroundUpperCased(jointPoint: AroundJointPoint) {
```

