import { AfterJointPoint, AroundJointPoint, BeforeJointPoint } from "./JointPoint"
import { Around, Before, After, Aspect } from "./Aspect";
import { Managed, Singleton } from "./InstanceManager";
import { TestGroup } from "yaf-test"

@Singleton()
class One {
    value: string
    constructor(value: string) {
        this.value = value
    }

    getValue() {
        return this.value
    }
}

@Managed('Foo')
class ManagedExampleClass {
    a: string = 'aaa'
    b: string

    constructor(b: string = 'b') {
        this.b = b
    }

    getValue(additional: string = '') {
        return this.a + this.b + additional
    }

    upperCased(a: string) {
        return a.toUpperCase()
    }
}

const managedExample = new ManagedExampleClass('ccc')

@Aspect
class TestAspect {
    protected prefix = 'from_cache'
    protected cache = new Map<string, any>()

    @Before(managedExample, '->getValue()')
    beforeGetValue(jointPoint: BeforeJointPoint) {
        jointPoint.setArguments(['set from before'])
    }

    @After(ManagedExampleClass, '->getValue()')
    afterGetValue(jointPoint: AfterJointPoint) {
        return jointPoint.getLastReturn().toUpperCase()
    }

    @Around('Foo->upperCased()')
    cacheAround(jointPoint: AroundJointPoint) {
        const id = jointPoint.getArguments().join('_')
        if (this.cache.has(id)) {
            return jointPoint.break(this.prefix + ':' + this.cache.get(id))
        }
        const ret = jointPoint.proceed()
        this.cache.set(id, ret)
        return ret
    }
}

const one = new One('one')
const two = new One('two')

TestGroup.Area('Singleton', test => {
    test('One value', one.getValue()).is('one')
    test('two value', two.getValue()).is('one')
    test('Same Instance', one).is(two)
})

TestGroup.Area('Aspects', test => {
    test('Before and After', managedExample.getValue('ddd')).is('AAACCCSET FROM BEFORE')
    test('Around', managedExample.upperCased)
        .executeMethod(managedExample, 'asdf').is('ASDF')
        .executeMethod(managedExample, 'asdfa').is('ASDFA')
        .executeMethod(managedExample, 'asdf').is('from_cache:ASDF')
})