import { AdviceChain, Link } from "./AdviceChain";
import { AspectType, CutExpression } from "./Aspect";
import { AspectManager } from "./AspectManager";
import { InstanceManagerClass, EntryManager, resetInstanceManagerInstanceTo, Decorator } from 'ts-instance-manager'
import { AroundJointPoint, BeforeJointPoint, AfterJointPoint } from "./JointPoint";

export interface AspectEntry {
    method: string,
    type: string,
    callback: any
}

export interface Entry {
    instance: any
    aspects: AspectEntry[]
}

class InstanceManager extends InstanceManagerClass {
    static PROP_ENTRY_IDENTIFIER = '__instance_manager_identifier'

    getEntry(key: string, strict: boolean = false) {
        return <Entry>super.getEntry(key, strict)
    }

    hasEntryWithProp(key: string, prop: string | Symbol) {
        if (!EntryManager.hasEntry(key)) {
            return false
        }
        return !!this.getEntry(key, true).aspects.find(aspect => aspect.method === prop)
    }

    createEmptyEntry(type: Decorator): Entry {
        return {
            instance: undefined,
            aspects: []
        }
    }

    createSingletonEntry(target: any, argumentArray: any[], newTarget: Function) {
        const entry = <Entry>super.createSingletonEntry(target, argumentArray, newTarget)
        entry.aspects = []
        return entry
    }

    getEntriesByType(key: string, type: AspectType) {
        return this.getEntry(key).aspects.filter(a => a.type === type)
    }

    getEntriesByTypeAndMethod(key: string, type: AspectType, method: string | Symbol) {
        return this.getEntry(key).aspects.filter(a => a.type === type && a.method === method)
    }

    getManagedHandler() {
        return {
            get: (target: any, prop: string | symbol, receiver: any) => {
                const identifier = InstanceManager.getEntryIdentifier(target)

                if (!this.hasEntryWithProp(identifier, prop)) {
                    return Reflect.get(target, prop, receiver)
                }

                const adviceChain = new AdviceChain()

                return (...args: any) => {

                    for (const aroundAspect of this.getEntriesByTypeAndMethod(identifier, AspectType.Around, prop)) {
                        const instance = AspectManager.getAspectInstanceByMethod(aroundAspect.callback)
                        const link = new Link((link) => aroundAspect.callback.apply(instance, [new AroundJointPoint(link)]))
                        adviceChain.addLink(link)
                    }

                    for (const beforeAspect of this.getEntriesByTypeAndMethod(identifier, AspectType.Before, prop)) {
                        const instance = AspectManager.getAspectInstanceByMethod(beforeAspect.callback)
                        const link = new Link((link) => beforeAspect.callback.apply(instance, [new BeforeJointPoint(link)]))
                        adviceChain.addLink(link)
                    }

                    adviceChain.addLink(new Link(link => {
                        return Reflect.get(target, prop, receiver).apply(target, link.getArguments())
                    }))

                    for (const afterAspect of this.getEntriesByTypeAndMethod(identifier, AspectType.After, prop)) {
                        const instance = AspectManager.getAspectInstanceByMethod(afterAspect.callback)
                        const link = new Link((link) => afterAspect.callback.apply(instance, [new AfterJointPoint(link)]))
                        adviceChain.addLink(link)
                    }

                    return adviceChain.run(args)
                }

            }
        }
    }

    addAspect(cutExpression: CutExpression, type: string, callback: any) {
        if (!EntryManager.hasEntry(cutExpression.identifier)) {
            return
        }

        this.getEntry(cutExpression.identifier).aspects.push({
            method: cutExpression.method,
            type: type,
            callback: callback
        })
    }
}
const InstanceManagerInstance = new InstanceManager()
resetInstanceManagerInstanceTo(InstanceManagerInstance)
export { InstanceManagerInstance as InstanceManager, InstanceManager as InstanceManagerClass }

export {
    Singleton,
    Managed,
    Inject
} from 'ts-instance-manager'