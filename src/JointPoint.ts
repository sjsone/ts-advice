import { Link } from "./AdviceChain"

export class AbstractJoinPoint {
    protected link: Link

    constructor(link: Link) {
        this.link = link
    }

    getLastReturn() {
        return this.link.getLastReturn()
    }

    getArguments() {
        return this.link.getArguments()
    }

    setArguments(args: any) {
        this.link.setArguments(args)
    }
}

export class BreakableAbstractJoinPoint extends AbstractJoinPoint {
    break(returnValue: any) {
        this.link.setProceeded(true)
        return returnValue
    }
}

export class BeforeJointPoint extends BreakableAbstractJoinPoint {
    proceed() {
        return this.link.proceed()
    }
}

export class AfterJointPoint extends AbstractJoinPoint{
    
}

export class AroundJointPoint extends BreakableAbstractJoinPoint {
    proceed() {
        return this.link.proceed()
    }
}