export class Link {
    protected callback: (link: Link) => any
    protected nextLink: Link | undefined
    protected lastReturn: any
    protected args: any = {}
    protected proceeded = false;

    constructor(callback: (link: Link) => any) {
        this.callback = callback
    }

    setNextLink(nextLink: Link) {
        this.nextLink = nextLink
    }

    run(args: any, setLastReturn: boolean = false, lastReturn: any = undefined): any {
        this.args = args
        if (setLastReturn) {
            this.lastReturn = lastReturn
        }
        this.lastReturn = this.callback(this)
        if (!this.proceeded) {
            return this.proceed()
        }
        return this.lastReturn
    }

    setLastReturn(lastReturn: any) {
        this.lastReturn = lastReturn
    }

    getLastReturn() {
        return this.lastReturn
    }

    getArguments() {
        return this.args
    }

    setArguments(args: any) {
        this.args = args
    }

    proceed(): any {
        this.proceeded = true
        return this.nextLink ? this.nextLink.run(this.args, true, this.lastReturn) : this.lastReturn
    }

    setProceeded(proceeded: boolean) {
        this.proceeded = proceeded
    }

    hasProceeded(): any {
        return this.proceeded
    }
}

export class AdviceChain {
    protected links: Link[]

    constructor() {
        this.links = []
    }

    addLink(link: Link) {
        if (this.links.length) {
            this.links[this.links.length - 1].setNextLink(link)
        }
        this.links.push(link)
    }

    run(args: any) {
        return this.links[0].run(args)
    }
}
