import { AspectEntry, AspectManager } from "./AspectManager"
import { InstanceManager, InstanceManagerClass } from "./InstanceManager"

export enum AspectType {
    Around = 'around',
    Before = 'before',
    After = 'after'
}

export interface CutExpression {
    identifier: string
    method: string
    arguments: string[]
}

const handleCutExpression = (rawExpression: string): CutExpression => {
    const [rawIdentifier, rawMethodCall] = rawExpression.split('->')
    const regexResult = (/(.+)\((.*)\)/).exec(rawMethodCall) // test(a,b) => ["test", "a,b"]
    if (regexResult === null) {
        throw new Error(`Could not parse expression "${rawExpression}" due to invalid method part`)
    }
    const rawMethodName = regexResult[1] ? regexResult[1] : '*'
    const rawMethodArguments = regexResult[2] ? regexResult[2] : ''

    const methodArguments = rawMethodArguments.split(',').map(a => a.trim()).filter(a => a)

    return {
        identifier: rawIdentifier,
        method: rawMethodName,
        arguments: methodArguments
    }
}

const resolveIdentifier = (value: any): string => {
    switch (typeof value) {
        case 'object': return value.constructor[InstanceManagerClass.PROP_ENTRY_IDENTIFIER]
        case 'function': return value[InstanceManagerClass.PROP_ENTRY_IDENTIFIER]
        default: return value
    }
}

/**
 * @type {ClassDecorator}
 * @param constructor 
 */
export function Aspect(constructor: new (...params: any[]) => any) {
    const entry: AspectEntry = {
        name: constructor.name,
        instance: new constructor(),
        methods: []
    }

    for (const key of Object.getOwnPropertyNames(constructor.prototype)) {
        if (key === 'constructor') {
            continue;
        }
        entry.methods.push(constructor.prototype[key])
    }

    AspectManager.addEntry(entry)
}

export function Advice(instanceOrClass: any, expression: any, type: AspectType): MethodDecorator {
    if (typeof instanceOrClass === 'string' && expression === undefined) {
        expression = instanceOrClass
        instanceOrClass = undefined
    } else if (expression === undefined) {
        throw new Error("Missing Expression in Advice.")
    }

    const cutExpression = handleCutExpression(expression)

    if (cutExpression.identifier === '') {
        if (instanceOrClass) {
            cutExpression.identifier = resolveIdentifier(instanceOrClass)
        } else {
            throw new Error("Missing identifier in Expression")
        }
    }

    return function (target: any, propertyKey: string | symbol, descriptor: PropertyDescriptor) {
        InstanceManager.addAspect(cutExpression, type, descriptor.value)
    }
}

export function Before(expression: string): MethodDecorator
export function Before(instanceOrClass: Function | Object, expression: string): MethodDecorator
export function Before(instanceOrClass: any, expression?: any): MethodDecorator {
    return Advice(instanceOrClass, expression, AspectType.Before)
}

export function After(expression: string): MethodDecorator
export function After(instanceOrClass: Function | Object, expression: string): MethodDecorator
export function After(instanceOrClass: any, expression?: any): MethodDecorator {
    return Advice(instanceOrClass, expression, AspectType.After)
}

export function Around(expression: string): MethodDecorator
export function Around(instanceOrClass: Function | Object, expression: string): MethodDecorator
export function Around(instanceOrClass: any, expression?: any): MethodDecorator {
    return Advice(instanceOrClass, expression, AspectType.Around)
}