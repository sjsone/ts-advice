
export interface AspectEntry {
    name: string
    instance: new (...params: any[]) => any
    methods: Function[]
}

class AspectManager {
    protected entries: { [key: string]: AspectEntry } = {}

    addEntry(entry: AspectEntry) {
        this.entries[entry.name] = entry
    }

    getAspectInstanceByMethod(method: Function) {
        const name = Object.getOwnPropertyNames(this.entries).find((name: string) => {
            const entry = this.entries[name]
            return entry.methods.includes(method)
        })
        if (name === undefined) {
            return undefined
        }
        return this.entries[name].instance
    }
}

const AspectManagerInstance = new AspectManager()
export { AspectManagerInstance as AspectManager }